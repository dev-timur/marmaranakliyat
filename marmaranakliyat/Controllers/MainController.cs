﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;

using System.Net.Mail;

namespace marmaranakliyat.Controllers
{
    public class MainController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        [ActionName("about")]
        public IActionResult About()
        {

            return View();
        }


        [ActionName("contact")]
        public IActionResult Contact()
        {
            return View();
        }

        [ActionName("contact-process")]
        [HttpPost]
        public IActionResult ContactProcess()
        {
            try
            {
                Mail m = new Mail(
                 Request.Form["name"].ToString(),
                 Request.Form["mail"].ToString(),
                 Request.Form["phone"].ToString(),
                 Request.Form["header"].ToString(),
                 Request.Form["message"].ToString()
                 );

                if (m.Name != null && m.MailAddress != null && m.Phone != null && m.Header != null)
                {
                    m.SendUserMessage();
                }
                ViewBag.Succes = "Teşekkürler Mailniz başarı bir şekilde gönderildi";

                return View("~/Views/Main/Index.cshtml");
            }
            catch (Exception)
            {
                ViewBag.Error = "Mesaj gönderilirken hata oluştu.";

                return View("main/contact");
            }
        }

        [ActionName("get-offer")]
        public IActionResult GetOffer()
        {
            return View();
        }

        [ActionName("offer-process")]
        [HttpPost]
        public IActionResult OfferProcess()
        {
            Offer offer = new Offer();
            try
            {
                Offer.Name = Request.Form["adsoyad"];
                Offer.Mail = Request.Form["eposta"];
                Offer.Phone = Request.Form["gsm"];
                Offer.TransportCity = Request.Form["alsehir"];
                Offer.TransportDistrict = Request.Form["alilce"];
                Offer.TransporBegintDate = Request.Form["bastarih"];
                Offer.TransporFinishDate = Request.Form["bittarih"];
                Offer.TransportState = Request.Form["alnasiltasinir"];
                Offer.TransportType = Request.Form["yapilacakis"];
                Offer.StuffState = Request.Form["esyadurum"];
                Offer.ArrivePackingState = Request.Form["gitnasiltasinir"];
                Offer.FloorState = Request.Form["alkatdurum"];
                Offer.PackingState = Request.Form["paketleme"];
                Offer.ArriveCity = Request.Form["gitsehir"];
                Offer.ArriveDistrict = Request.Form["gitilce"];
                Offer.ArriveFloorState = Request.Form["gitkatdurum"];
                Offer.ArriveDescription = Request.Form["aciklama"];

                if (
                    Offer.Name != null &&
                    Offer.Mail != null &&
                    Offer.Phone != null &&
                    Offer.TransportCity != null &&
                    Offer.TransportDistrict != null &&
                    Offer.TransporBegintDate != null &&
                    Offer.TransporFinishDate != null &&
                    Offer.TransportType != null &&
                    Offer.StuffState != null &&
                    Offer.TransportState != null &&
                    Offer.FloorState != null &&
                    Offer.PackingState != null &&
                    Offer.ArriveCity != null &&
                    Offer.ArriveDistrict != null &&
                    Offer.ArriveFloorState != null &&
                    Offer.ArriveDescription != null
                    )
                {
                    offer.SendOffer();
                }
                return View("/");
            }
            catch (Exception)
            {
                ViewBag.Error("Teklifiniz gönderilirken bir hata oluştu.Lütfen sonra tekrar deneyiniz");
                return View("~/Views/Main/Index.cshtml");
            }
        }

        [ActionName("faq")]
        public IActionResult Faq()
        {

            return View();
        }

        [ActionName("gallery")]
        public IActionResult Gallery()
        {

            return View();
        }

        [ActionName("video-gallery")]
        public IActionResult VideoGallery()
        {

            return View();
        }

        [ActionName("service-home-carrying")]
        public IActionResult ServiceHomeCarrying()
        {

            return View();
        }

        [ActionName("service-lift-carrying")]
        public IActionResult ServiceLiftCarrying()
        {

            return View();
        }

        [ActionName("service-office-carrying")]
        public IActionResult ServiceOfficeCarrying()
        {

            return View();
        }

        [ActionName("service-stuff-storage")]
        public IActionResult ServiceStuffStorage()
        {

            return View();
        }

        [ActionName("service-insured-carrying")]
        public IActionResult ServiceInsuredCarrying()
        {

            return View();
        }

        [ActionName("service-intercity-carrying")]
        public IActionResult ServiceIntercityCarrying()
        {

            return View();
        }

        [ActionName("vision")]
        public IActionResult Vision()
        {

            return View();
        }
        [ActionName("mission")]
        public IActionResult Mission()
        {

            return View();
        }
    }
}
