﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace System
{
    public class Mail
    {
        public string Name { get; set; }
        public string MailAddress { get; set; }
        public string HostMailAdress { get; set; }
        public string Phone { get; set; }
        public string Header { get; set; }
        public  string Message { get; set; }        
        
        public int HostPort { get => 587; }
        public Mail(string GetPerson, string GetMailAdress, string GetPhone, string GetHeader, string GetMessage)
        {
            this.Name = GetPerson;
            this.MailAddress = GetMailAdress;
            this.Phone = GetPhone;
            this.Header = GetHeader;
            this.Message = GetMessage;
        }
        private string CreateMailBody ()
        {
            string HtmlTemplatePath = ".\\MailTemplates\\ContactTemplate.html";
            string body = string.Empty;
            using (StreamReader reader = File.OpenText(HtmlTemplatePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("@Mail.Name",Name).Replace("@Mail.Phone",Phone).Replace("@Mail.MailAddress",MailAddress).Replace("@Mail.Message",Message);
            return body;
        }
        public void SendUserMessage()
        {            
            MailMessage msg = new MailMessage();
            msg.Subject = "Marmara Nakliyat İletişim Formu";
            msg.From = new MailAddress(MailAddress.ToString(), Name.ToString());
            msg.To.Add(new MailAddress("marmaranakliyatoffical@gmail.com", "Marmara Nakliyat"));            
            msg.IsBodyHtml = true;
            msg.Body = CreateMailBody();            
            msg.Priority = MailPriority.High;
            
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            NetworkCredential AccountInfo = new NetworkCredential("marmaranakliyatoffical@gmail.com", "5bdc899086");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = AccountInfo;
            smtp.EnableSsl = true;
            smtp.Send(msg);
        }        
    }
}
