﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace System
{
    public class Offer
    {
        public static string TransporBegintDate { get; set; }
        public static string TransporFinishDate { get; set; }
        public static string TransportType { get; set; }
        public static string TransportCity { get; set; }
        public static string TransportDistrict { get; set; }
        public static string StuffState { get; set; }
        public static string TransportState { get; set; }
        public static string FloorState { get; set; }
        public static string PackingState { get; set; }
        public static string ArriveCity { get; set; }
        public static string ArriveDistrict { get; set; }
        public static string ArrivePackingState { get; set; }
        public static string ArriveFloorState { get; set; }
        public static string ArriveDescription { get; set; }
        public static string Name { get; set; }
        public static string Mail { get; set; }
        public static string Phone { get; set; }
        public static bool Verifaction { get; set; }
        public IWebHostEnvironment wb { get; set; }
        public Offer() { }
       
        public string CreateMailBody()
        {          
            string HtmlBody = string.Empty;
            string HTMLTemplatePath = ".\\MailTemplates\\OfferTemplate.html";
            string HtmlTemplate = File.ReadAllText(HTMLTemplatePath);

            using (StreamReader reader = File.OpenText(HTMLTemplatePath))
            {
                HtmlBody = reader.ReadToEnd();
            }
            HtmlBody = HtmlBody.Replace("@Offer.Mail", Mail).Replace("@Offer.Name", Name);
            HtmlBody = HtmlBody.Replace("@Offer.Phone", Phone.ToString()).Replace("@Offer.TransportCity/@Offer.TransportDistrict", TransportCity +"/"+ TransportDistrict);
            HtmlBody = HtmlBody.Replace("@Offer.StuffState", StuffState).Replace("@Offer.TransportState", TransportState).Replace("@Offer.FloorState",FloorState);
            HtmlBody = HtmlBody.Replace("@Offer.TransporBegintDate ", TransporBegintDate.ToString()).Replace("@Offer.TransporFinishDate",TransporFinishDate.ToString());
            HtmlBody = HtmlBody.Replace("@Offer.ArriveCity/@OfferArriveDistrict", ArriveCity+"/"+ArriveDistrict).Replace("@Offer.ArrivePackingState", ArrivePackingState);
            HtmlBody = HtmlBody.Replace("@Offer.ArriveFloorState", ArriveFloorState).Replace("@Offer.ArriveDescription", ArriveDescription);
            return HtmlBody;
        }
        public void SendOffer()
        {
            MailMessage msg = new MailMessage();
            msg.Subject = "Marmara Nakliyat Teklifi";
            msg.From = new MailAddress(Mail.ToString(), Name.ToString());
            msg.To.Add(new MailAddress("marmaranakliyatoffical@gmail.com", "Marmara Nakliyat"));            
            msg.IsBodyHtml = true;
            msg.Body = CreateMailBody();            
            msg.Priority = MailPriority.High;
            
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            NetworkCredential AccountInfo = new NetworkCredential("marmaranakliyatoffical@gmail.com", "5bdc899086");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = AccountInfo;
            smtp.EnableSsl = true; 
            smtp.Send(msg);
        }
    }
}
