﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace System
{
    public class Organization
    {
        public static string Title { get => "Marmara Nakliyat Ev Ofis Taşıma Limited Şirketi"; }
        public static string Name { get => "Marmara Nakliyat"; }
        public static string PrimaryPhone { get => "0532 790 90 93"; }
        public static string PrimaryAddress { get=> "Fındıklı Mah. Mehmet Akif Ersoy Cad. No:2 İçkapı No:1 Maltepe/İstanbul"; }
        public static string PrimaryRawPhone { get => "+905327909093"; }       
        public static string SocialMediaInstagramUri { get => "https://instagram.com/marmaraevdenevenakliyat?utm_medium=copy_link"; }
        public static string SocialMediaLinkedInUri { get => ""; }
        public static string SocialMediaFacebookUri { get => "https://www.facebook.com/nakliyatmarmara/"; }
        public static string SocialMediaYoutubeUri { get => "Hazırlanıyor..."; }
        public static string PrimaryMail { get => "info@marmaranakliye.com.tr"; }
        public static string seconmaryMail { get => "info@marmaranakliyat.com.tr"; }
        public static string WhatsappMessage { get => "https://api.whatsapp.com/send/?phone=+905327909093&text=Sizlere%20internet%20sitenizden%20ulaşıyorum."; }

    }

}